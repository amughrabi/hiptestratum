package com.hiptestratum.hiptest;

import java.util.ArrayList;
import java.util.List;

public class DiffResult {

    private List<Action> newActions = new ArrayList<>();
    private List<Action> nonChanged = new ArrayList<>();
    private List<Action> deleted = new ArrayList<>();
    private List<Action> finalActions;
    private List<Action> modified;

    public List<Action> getNewActions() {
        return newActions;
    }

    public void setNewActions(List<Action> newActions) {
        this.newActions = newActions;
    }

    public List<Action> getNonChanged() {
        return nonChanged;
    }

    public void setNonChanged(List<Action> nonChanged) {
        this.nonChanged = nonChanged;
    }

    public List<Action> getDeleted() {
        return deleted;
    }

    public void setDeleted(List<Action> deleted) {
        this.deleted = deleted;
    }

    public List<Action> getFinalActions() {
        return finalActions;
    }

    public void setFinalActions(List<Action> finalActions) {
        this.finalActions = finalActions;
    }

    public List<Action> getModified() {
        return modified;
    }

    public void setModified(List<Action> modified) {
        this.modified = modified;
    }
}
