package com.hiptestratum.hiptest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ActionImporter {

    private final File file;
    private static ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

    public ActionImporter(File file) {
        this.file = file;
    }

    public File getFile() {
        return file;
    }


    public List<Action> get() throws IOException {
        return constructActions(mapper.readValue(file, ArrayList.class));
    }

    private List<Action> constructActions(List data) {
        List<Action> actions = new ArrayList<>();

        for (Object obj: data) {
            Action action = new Action();
            Map map = (Map) obj;
            action.setName((String) map.get("name"));
            action.setUuid((String) map.get("uid"));
            action.setBodyHash((String) map.get("body_hash"));

            List parameters = (List) map.get("parameters");

            List<String> params = new ArrayList<>();

            if (parameters != null) {
                for (Object parameter : parameters) {
                    Map param = (Map) parameter;
                    params.add((String) param.values().iterator().next());
                }
            }

            action.setBodyHash((String) map.get("body_hash"));
            action.setParameters(params);

            actions.add(action);
        }

        return actions;
    }
}
