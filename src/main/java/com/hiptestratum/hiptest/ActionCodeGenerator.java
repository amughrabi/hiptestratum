package com.hiptestratum.hiptest;

import java.util.List;

public class ActionCodeGenerator {

    private final List<Action> actions;
    private final GeneratorConfig config;

    public ActionCodeGenerator(GeneratorConfig config, List<Action> actions) {
        this.config = config;
        this.actions = actions;
    }

    public List<Action> getActions() {
        return actions;
    }

    public String getClassName() {
        return config.getActionwordsClassname();
    }

    public void setClassName(String className) {
        this.config.setActionwordsClassname(className);
    }

    public String getExtendsName() {
        return config.getExtendsClassName();
    }

    public void setExtendsName(String extendsName) {
        config.setExtendsClassName(extendsName);
    }

    public String getPackageName() {
        return config.getPackageName();
    }

    public void setPackageName(String packageName) {
        this.config.setPackageName(packageName);
    }

    public String generate() {
        StringBuilder builder = new StringBuilder();

        if (!config.isMethodsOnly()) {
            if (config.getPackageName() != null && config.getPackageName().length() != 0) {
                builder.append("package ").append(config.getPackageName()).append(";");
                builder.append("\n");
                builder.append("\n");
            }

            builder.append("public class ").append(config.getActionwordsClassname());

            if (config.getExtendsClassName() != null && config.getExtendsClassName().length() != 0) {
                builder.append(" extends ").append(config.getExtendsClassName());
            }

            builder.append(" {").append("\n");
        }

        for (Action action : actions) {
            builder.append("\n");
            builder.append("public void ").append(Util.rename(action.getName())).append("(");

            for (int i = 0; i < action.getParameters().size(); i++) {
                builder.append("String ").append(action.getParameters().get(i));

                if (i != action.getParameters().size() - 1) {
                    builder.append(", ");
                }
            }

            builder.append(")  { // UUID: ").append(action.getUuid()).append("\n");

            builder.append("}");
            builder.append("\n");
        }

        if (!config.isMethodsOnly()) {
            builder.append("}");
        }

        return builder.toString();
    }
}
