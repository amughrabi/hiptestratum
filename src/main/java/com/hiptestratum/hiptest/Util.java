package com.hiptestratum.hiptest;

public class Util {

    public static String rename(String name) {
        name = name.replace("_", " ");
        name = name.replace("$", "");
        name = name.replace("-", "");
        name = name.replace("'", "");
        name = name.replace("\"", "");
        name = name.replace("{", "");
        name = name.replace("/", "");
        name = name.replace("}", "");
        while (name.contains("  ")) {
            name = name.replace("  ", " ");
        }
        return toCamelCase(name);
    }

    public static String toCamelCase(String s){
        String[] parts = s.split(" ");
        StringBuilder camelCaseString = new StringBuilder();

        for (int i = 0; i < parts.length; i++) {
            if (i == 0) {
                camelCaseString.append(parts[i].toLowerCase());
                continue;
            }

            camelCaseString.append(toProperCase(parts[i]));
        }

        return camelCaseString.toString();
    }

    public static String toProperCase(String s) {
        return s.substring(0, 1).toUpperCase() +
                s.substring(1).toLowerCase();
    }

    public static boolean isNullEmptyOrWhiteSpace(String value) {
        return value == null || value.trim().length() < 1;
    }

    public static String noNull(String body) {
        return body == null? "": body;
    }
}
