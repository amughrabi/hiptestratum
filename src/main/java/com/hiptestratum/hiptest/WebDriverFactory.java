package com.hiptestratum.hiptest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class WebDriverFactory {
    static {
        System.setProperty("webdriver.gecko.driver", System.getProperties().get("basedir") +
                "/src/test/resources/selenium_standalone_binaries/linux/marionette/64bit/geckodriver"
        );
        System.setProperty("webdriver.chrome.driver", System.getProperties().get("basedir") +
                "/src/test/resources/selenium_standalone_binaries/linux/googlechrome/64bit/chromedriver");
    }

    public static enum EDriverType {
        CHROME {
            @Override
            public String toString() {
                return "CHROME";
            }
        },
        FIREFOX {
            @Override
            public String toString() {
                return "FIREFOX";
            }
        }
    }

    public static WebDriver get(EDriverType type) {
        switch (type) {
            case CHROME:
                return new ChromeDriver();
            case FIREFOX:
                return new FirefoxDriver();
        }
        return new ChromeDriver();
    }
}
