package com.hiptestratum.hiptest;

import java.io.File;
import java.io.IOException;

public class CodeMerger {

    private final DiffResult result;
    private final File original;
    private final GeneratorConfig config;

    public CodeMerger(GeneratorConfig config, DiffResult result, File original) {
        this.config = config;
        this.result = result;
        this.original = original;
    }

    public DiffResult getResult() {
        return result;
    }

    public File getOriginal() {
        return original;
    }

    public String merge() throws IOException {
        ParsedClass parsedClass = ParsedClass.from(original);

        for (Action action : result.getFinalActions()) {
            parsedClass.add(action);
        }

        for (Action action : result.getNewActions()) {
            parsedClass.add(action, "new method");
        }

        for (Action action : result.getModified()) {
            parsedClass.add(action, "modified method");
        }

        MethodCodeGenerator generator = new MethodCodeGenerator(new GeneratorConfig(config).setMethodsOnly(false), parsedClass);
        return generator.generate();
    }
}
