package com.hiptestratum.hiptest;

public class GeneratorConfig {

    private String packageName;
    private String extendsClassName;
    private String actionwordsClassname;
    private boolean methodsOnly;
    private boolean throwOnNonImplemented;

    public GeneratorConfig() {}

    public GeneratorConfig(GeneratorConfig config) {
        this.packageName = config.getPackageName();
        this.extendsClassName = config.getExtendsClassName();
        this.actionwordsClassname = config.getActionwordsClassname();
        this.methodsOnly = isMethodsOnly();
    }

    public String getPackageName() {
        return packageName;
    }

    public GeneratorConfig setPackageName(String packageName) {
       this.packageName = packageName;
       return this;
    }

    public String getExtendsClassName() {
        return extendsClassName;
    }

    public GeneratorConfig setExtendsClassName(String extendsClassName) {
        this.extendsClassName = extendsClassName;
        return this;
    }

    public String getActionwordsClassname() {
        return actionwordsClassname;
    }

    public GeneratorConfig setActionwordsClassname(String actionwordsClassname) {
        this.actionwordsClassname = actionwordsClassname;
        return this;
    }

    public boolean isMethodsOnly() {
        return methodsOnly;
    }

    public GeneratorConfig setMethodsOnly(boolean methodsOnly) {
        this.methodsOnly = methodsOnly;
        return this;
    }

    public boolean isThrowOnNonImplemented() {
        return throwOnNonImplemented;
    }

    public void setThrowOnNonImplemented(boolean throwOnNonImplemented) {
        this.throwOnNonImplemented = throwOnNonImplemented;
    }
}
