package com.hiptestratum.hiptest;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DiffProcessor {



    private final File original;
    private final File updated;

    public DiffProcessor(File original, File updated) {
        this.original = original;
        this.updated = updated;
    }

    public DiffResult process() throws Exception {
        DiffResult result = new DiffResult();

        List<Action> originalActions = new ActionImporter(original).get();
        List<Action> updatedActions = new ActionImporter(updated).get();

        result.setDeleted(getDeleted(originalActions, updatedActions));
        result.setNonChanged(getNonChanged(originalActions, updatedActions));
        result.setNewActions(getNewActions(originalActions, updatedActions));
        result.setModified(getModified(originalActions, updatedActions));
        result.setFinalActions(updatedActions);

        return result;
    }

    private List<Action> getDeleted(List<Action> originalActions, List<Action> updatedActions) {
        List<Action> result = new ArrayList<>();

        for (Action originalAction : originalActions) {
            boolean found = false;
            for (Action updatedAction : updatedActions) {
                if (originalAction.getUuid().equals(updatedAction.getUuid())) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                result.add(originalAction);
            }
        }
        return result;
    }

    private List<Action> getNonChanged(List<Action> originalActions, List<Action> updatedActions) {
        List<Action> result = new ArrayList<>();

        for (Action updatedAction : updatedActions) {
            for (Action originalAction : originalActions) {
                if (originalAction.getUuid().equals(updatedAction.getUuid()) && originalAction.getBodyHash().equals(updatedAction.getBodyHash())) {
                    result.add(originalAction);
                    break;
                }
            }
        }
        return result;
    }

    private List<Action> getNewActions(List<Action> originalActions, List<Action> updatedActions) {
        List<Action> result = new ArrayList<>(updatedActions);

        for (Iterator<Action> iterator = result.iterator(); iterator.hasNext();) {
            Action updated = iterator.next();
            for (Action originalAction : originalActions) {
                if (originalAction.getUuid().equals(updated.getUuid())) {
                    iterator.remove();
                    break;
                }
            }
        }

        return result;
    }

    private List<Action> getModified(List<Action> originalActions, List<Action> updatedActions) {
        List<Action> result = new ArrayList<>(updatedActions);

        for (Iterator<Action> iterator = result.iterator(); iterator.hasNext();) {
            Action updated = iterator.next();
            for (Action originalAction : originalActions) {
                if (originalAction.getUuid().equals(updated.getUuid()) && originalAction.getBodyHash().equals(updated.getBodyHash())) {
                    iterator.remove();
                    break;
                }
            }
        }

        return result;
    }
}
