package com.hiptestratum.hiptest;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParsedClass {

    private static Pattern PACKAGE = Pattern.compile("package\\s+(.+);");
    private static Pattern IMPORT = Pattern.compile("import\\s+(.+);");
    private static Pattern CLASSNAME = Pattern.compile("class\\s+(\\w+)\\s+(extends\\s+(\\w+))?");
    private static Pattern METHOD = Pattern.compile("(private|public)\\s+void\\s+(\\w+)\\(([^)]*)\\)\\s*(throws\\s*\\w+)?\\s+\\{?\\s*(//\\s+UUID:\\s+([^\\s]+))?");
    private static Pattern PARAM = Pattern.compile("(\\w+)\\s+(\\w+)");

    private String className;
    private String packageName;
    private List<String> imports = new ArrayList<>();
    private Set<Method> methods = new LinkedHashSet<>();
    private String extendsClassname;

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public List<String> getImports() {
        return imports;
    }

    public void setImports(List<String> imports) {
        this.imports = imports;
    }

    public Set<Method> getMethods() {
        return methods;
    }

    public void setMethods(Set<Method> methods) {
        this.methods = methods;
    }

    public static ParsedClass from(File original) throws IOException {
        ParsedClass parsedClass = new ParsedClass();

        List<String> lines = Files.readAllLines(original.toPath());

        for (Iterator<String> iterator = lines.iterator(); iterator.hasNext();) {
            String line = iterator.next();

            Matcher matcher = PACKAGE.matcher(line);
            if (matcher.find()) {
                parsedClass.packageName = matcher.group(1);
            }

            matcher = IMPORT.matcher(line);
            if (matcher.find()) {
                parsedClass.imports.add(matcher.group(1));
            }

            matcher = CLASSNAME.matcher(line);
            if (matcher.find()) {
                parsedClass.className = matcher.group(1);
                parsedClass.extendsClassname = matcher.group(3);
            }

            matcher = METHOD.matcher(line);
            if (matcher.find()) {
                Method method = new Method();
                method.setPublic(matcher.group(1).equals("public"));
                method.setName(matcher.group(2));
                method.setUuid(matcher.group(6));

                Matcher paramMatcher = PARAM.matcher(matcher.group(3));

                while (paramMatcher.find()) {
                    Parameter parameter = new Parameter();
                    parameter.setType(paramMatcher.group(1));
                    parameter.setName(paramMatcher.group(2));
                    method.addParam(parameter);
                }


                StringBuilder body = new StringBuilder();

                int level = 0;

                for (;iterator.hasNext();) {
                    String methodLine = iterator.next();

                    if (methodLine.contains("{")) {
                        ++level;
                    }

                    if (methodLine.contains("}")) {
                        --level;
                    }

                    if (level == -1) {
                        break;
                    }

                    body.append(methodLine).append("\n");
                }
                method.setBody(body.toString());

                parsedClass.methods.add(method);
            }
         }

        return parsedClass;
    }

    public void add(Action action) {
        add(action, null);
    }

    public void add(Action action, String message) {
        if (action == null)
            return;

        Method method = new Method();
        method.setName(Util.rename(action.getName()));
        method.setaPublic(true);
        method.setComment(message);
        method.setUuid(action.getUuid());

        for (String parameter : action.getParameters()) {
            Parameter param = new Parameter();
            param.setName(parameter);
            param.setType("String");
            method.addParam(param);
        }

        // merge if found
        boolean found = false;
        for (Method m1 : methods) {
            if (method.getUuid().equals(m1.getUuid())) {
                m1.setName(method.getName());
                m1.setAction(method.getAction());
                m1.setComment(method.getComment());
                m1.setParameters(method.getParameters());
                found = true;
                break;
            }
        }

        // otherwise add
        if (!found) {
            methods.add(method);
        }
    }
}
