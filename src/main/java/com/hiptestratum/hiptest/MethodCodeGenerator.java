package com.hiptestratum.hiptest;

public class MethodCodeGenerator {

    private final ParsedClass parsedClass;
    private final GeneratorConfig config;

    public MethodCodeGenerator(GeneratorConfig config, ParsedClass parsedClass) {
        this.config = config;
        this.parsedClass = parsedClass;
    }

    public ParsedClass getParsedClass() {
        return parsedClass;
    }

    public String getClassName() {
        return config.getActionwordsClassname();
    }

    public void setClassName(String className) {
        this.config.setActionwordsClassname(className);
    }

    public String getExtendsName() {
        return config.getExtendsClassName();
    }

    public void setExtendsName(String extendsName) {
        config.setExtendsClassName(extendsName);
    }

    public String getPackageName() {
        return config.getPackageName();
    }

    public void setPackageName(String packageName) {
        this.config.setPackageName(packageName);
    }

    public String generate() {
        StringBuilder builder = new StringBuilder();

        if (!config.isMethodsOnly()) {
            if (config.getPackageName() != null && config.getPackageName().length() != 0) {
                builder.append("package ").append(config.getPackageName()).append(";");
                builder.append("\n");
                builder.append("\n");
            }

            for (String importedClass : parsedClass.getImports()) {
                builder.append("import ").append(importedClass).append(";");
                builder.append("\n");
            }

            builder.append("\n");
            builder.append("public class ").append(config.getActionwordsClassname());

            if (config.getExtendsClassName() != null && config.getExtendsClassName().length() != 0) {
                builder.append(" extends ").append(config.getExtendsClassName());
            }

            builder.append(" {").append("\n");
        }

        for (Method method : parsedClass.getMethods()) {
            builder.append("\n");

            if (method.getComment() != null) {
                builder.append("    // ").append(method.getComment());
                builder.append("\n");
            }

            builder.append("    public void ").append(method.getName()).append("(");

            for (int i = 0; i < method.getParameters().size(); i++) {
                builder.append(method.getParameters().get(i).getType()).append(" ").append(method.getParameters().get(i).getName());

                if (i != method.getParameters().size() - 1) {
                    builder.append(", ");
                }
            }

            builder.append(") throws Exception {");

            if (!Util.isNullEmptyOrWhiteSpace(method.getUuid())) {
                builder.append(" // UUID: ").append(method.getUuid());
            }

            builder.append("\n");

            if (!Util.isNullEmptyOrWhiteSpace(method.getBody())) {
                builder.append(method.getBody());
            } else if (config.isThrowOnNonImplemented()) {
                builder.append("        throw new UnsupportedOperationException();");
            }

            builder.append("    }");
            builder.append("\n");
        }

        if (!config.isMethodsOnly()) {
            builder.append("}");
        }

        return builder.toString();
    }
}
