package com.hiptestratum.hiptest;

import java.util.ArrayList;
import java.util.List;

public class Method {

    private String name;
    private String uuid;
    private String body;
    private Action action;
    private List<Parameter> parameters = new ArrayList<>();
    private boolean aPublic;
    private String comment;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public void setPublic(boolean aPublic) {
        this.aPublic = aPublic;
    }

    public boolean isPublic() {
        return aPublic;
    }

    public void setaPublic(boolean aPublic) {
        this.aPublic = aPublic;
    }

    public List<Parameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<Parameter> parameters) {
        this.parameters = parameters;
    }

    public void addParam(Parameter parameter) {
        this.parameters.add(parameter);
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Method method = (Method) o;

        if (aPublic != method.aPublic) return false;
        if (name != null ? !name.equals(method.name) : method.name != null) return false;
        if (action != null ? !action.equals(method.action) : method.action != null) return false;
        return (parameters != null ? parameters.equals(method.parameters) : method.parameters == null);
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (action != null ? action.hashCode() : 0);
        result = 31 * result + (parameters != null ? parameters.hashCode() : 0);
        result = 31 * result + (aPublic ? 1 : 0);
        return result;
    }
}
