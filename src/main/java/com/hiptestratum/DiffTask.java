package com.hiptestratum;

import com.hiptestratum.hiptest.*;
import org.apache.tools.ant.BuildException;

import java.io.File;
import java.nio.file.Files;

/**
 * Created by msamman on 7/12/17.
 */
public class DiffTask {

    private String originalYaml;
    private String updatedYaml;
    private String actionwordsName = "Actionwords";
    private String packageName;
    private String actionwordsPath;

    DiffTask(String []args) throws BuildException {
        originalYaml=args[0];
        updatedYaml=args[1];
        actionwordsPath=args[2];
        packageName=args[3];

    }

    public void execute() {
        GeneratorConfig config = new GeneratorConfig();

        config.setActionwordsClassname(actionwordsName);
        config.setPackageName(packageName);
        config.setThrowOnNonImplemented(true);

        File originalYaml = new File(this.originalYaml);
        File updatedYaml = new File(this.updatedYaml);

        File actionwordsFile = new File(actionwordsPath);

        try {
            DiffResult result = new DiffProcessor(originalYaml, updatedYaml).process();

            ActionCodeGenerator generator = new ActionCodeGenerator(config, result.getNewActions());

            generator.setClassName(getActionwordsName());

            generator.setExtendsName(AbstractActionwords.class.getCanonicalName());

            generator.setPackageName(getPackageName());

            CodeMerger merger = new CodeMerger(config, result, actionwordsFile);

            String out = merger.merge();

            Files.write(actionwordsFile.toPath(), out.getBytes());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public String getOriginalYaml() {
        return originalYaml;
    }

    public void setOriginalYaml(String originalYaml) {
        this.originalYaml = originalYaml;
    }

    public String getUpdatedYaml() {
        return updatedYaml;
    }

    public void setUpdatedYaml(String updatedYaml) {
        this.updatedYaml = updatedYaml;
    }

    public String getActionwordsName() {
        return actionwordsName;
    }

    public void setActionwordsName(String actionwordsName) {
        this.actionwordsName = actionwordsName;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getActionwordsPath() {
        return actionwordsPath;
    }

    public void setActionwordsPath(String actionwordsPath) {
        this.actionwordsPath = actionwordsPath;
    }
}
