package com.hiptestratum;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Actionwords extends com.hiptestratum.hiptest.AbstractActionwords {

    public void iSearchForKeyword(String keyword) throws Exception { // UUID: 128e6b6e-bb9f-4b73-b9de-77241aeefdd7
        // this is to test our web site
        WebElement element = driver.findElement(By.name("q"));
        element.clear();
        element.sendKeys(keyword);
        element.submit();
    }

    public void searchEngineIsUrl(String url) throws Exception { // UUID: cb912b93-27d6-4f54-b548-de3e57b7d28c
        driver.get(url);

    }

    public void aLinkToResIsShownInResults(String res) throws Exception { // UUID: 6e4bd670-ea37-44cf-95ab-4981bc467ba7
        (new WebDriverWait(driver, 100)).until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElements(By.linkText(res)).size() != 0;
            }
        });

        driver.close();
    }
}