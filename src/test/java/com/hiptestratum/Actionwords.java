package com.hiptestratum;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Actionwords {

    private WebDriver driver;
    static {
        System.setProperty("webdriver.gecko.driver", System.getProperties().get("basedir") +
                "/src/test/resources/selenium_standalone_binaries/linux/marionette/64bit/geckodriver"
        );
    }

    Actionwords() {
        driver = new FirefoxDriver();
    }

    public void iOpenHttpwwwgooglecom() {
        driver.get("http://www.google.com");
    }

    public void iSearchForAtypon() {
        WebElement element = driver.findElement(By.name("q"));
        element.clear();
        element.sendKeys("Atypon");
        element.submit();
    }

    public void aLinkToAtyponSoftwareEssentialToTheBusinessOfOnlinePublishingIsShownInTheResults() {
        final String matcher = "Atypon: Software essential to the business of online ...";
        (new WebDriverWait(driver, 10)).until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return webDriver.findElements(By.linkText(matcher)).size() != 0;
            }
        });

        driver.close();
    }
}