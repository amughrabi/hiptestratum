#!/bin/sh
# If geckodriver is not installed, please install
installGeckoDriver() {
    echo "Downloading GeckoDriver from https://github.com/mozilla/geckodriver ..."
    wget https://github.com/mozilla/geckodriver/releases/download/v0.13.0/geckodriver-v0.13.0-linux64.tar.gz
    echo "Extracting geckodriver-v0.13.0-linux64.tar.gz ..."
    tar -xvzf geckodriver-v0.13.0-linux64.tar.gz
    echo "Changing permission ..."
    chmod +x geckodriver
    echo "Making directory ~/apps64/geckodriver/"
    mkdir -p $HOME/apps64/geckodriver/
    echo "Moving geckodriver to ~/apps64/geckodriver/"
    mv geckodriver $HOME/apps64/geckodriver/
    echo "Adding geckodriver to Environment variables $PATH"
    export PATH=$PATH:$HOME/apps64/geckodriver
}

# Installing all dependencies.
##### If the
# Figlet - For development machine use.
# geckodriver
command -v geckodriver >/dev/null 2>&1 || { installGeckoDriver >&2; };
# chromedriver
command -v chromedriver >/dev/null 2>&1 || { installGeckoDriver >&2; };

# https://chromedriver.storage.googleapis.com/2.29/chromedriver_linux64.zip